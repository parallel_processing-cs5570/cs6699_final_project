# CS6699 Project Summary

A general summary of the work performed and general comments on the ChessEngine project is provided here.


## Work Performed

In short, the majority of the source code for the project was evaluated and annotated. Evaluation consisted of:
+ Decompressing the source code so that loops, breaks, definition start and end, etc. were clearly and easily found.
+ Addition and modification to existing comments for documentation and extension/maintainability purposes
+ Passing non-integral data types by constant reference (`const Foo&`) instead of by value (and non-const), where appropriate.
+ Restructuring the directory structure of the project for improved clarity
+ Expansion of `CMakeLists.txt` for use in the entirety of the project, not simply within the `src` directory.


## Application Style

The style present in the source prior to any modification was fairly consistent. Camel case was generally used for typesetting the variable and method names. Naming conventions were relatively decent, however some names were given little attention when they were made.


## Commentation and Documentation

Documentation of class methods was generally existant, as well as that for namespaces. Further documentation was added for the namespaces, classes, and class members modified. Beyond in-source comments for documentation purposes, little meaningful comments existed.

Documentation for this project may be [found here](https://parallel_processing-cs5570.gitlab.io/cs6699_final_project/).


## Class Diagram

No class diagram was generated for this project by hand. Instead, the `doxygen` tool was used to generate both the application documentation (see above) as well as the class diagram of the project. I was surprised at how few classes existed for the project, where interfaces were wrapped into `namespace`s. Further, classes were generally fairly large.

> To generate the documentation and class diagrams, enter this repository from a `shell` of some sort and enter `doxygen doxygen.cfg`. Use a web browser to open the documentation that will appear in the `index.html` file in the created `documentation` directory after performing this.


## ToDo Items

Items that still need worked on are indicated using either a `TODO` label or a `@todo` label. Items listed in the source code with a `@todo` label appear in the documentation generated for the project.


