/**
 * @file console.cpp
 * @brief Console class implementation
 * @author Clint B
 * @date 1/24/2019
 */
#include "console.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include "coutsync.h"
#include "debug.h"
#include "enginetools.h"
#include "graphics.h"
#include "move.h"
#include "position.h"
#include "types.h"


// Prints the contents of a single bitboard
void Console::printBitboard(Bitboard bb) noexcept
{
    // Iterate over the entire board (lower-left to upper-right)
    for (int row = 7; row >= 0; row--)
    {
        for (int col = 0; col < 8; col++)
        {
            // Print the square
            std::cout << " " << ((bb >> (8 * row + col)) & 1) << "";
        }
        // End the row
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


// Prints the combined content of bitbouards for display
void Console::printCombinedBitboards(std::shared_ptr<Position> board) noexcept
{
    char displayBoard[8][8];
    Graphics::createDisplayBitboard(board, displayBoard);

    // Iterate over board (lower-left to upper-right)
    for (int row = 7; row >= 0; row--)
    {
        // Create string buffer for the row and fill
        std::stringbuf rowChars;
        for (int col = 0; col < 8; col++)
        {
            // Use the piece specifier or a '.'
            rowChars.sputc(' ');
            if (displayBoard[row][col] != 0)
            {
                rowChars.sputc(displayBoard[row][col]);
            }
            else
            {
                rowChars.sputc('.');
            }
        }

        // Print the board's row
        syncCout << rowChars.str() << syncEndl;
    }
    syncCout << syncEndl;
}


// Prints the start message to show when the program launches
void Console::printStartMsg() noexcept
{
    syncCout << EngineTools::getEngineName()
             << " by " << EngineTools::getAuthorList() << syncEndl;

    // print mode message, label it as a warning if the engine is in Debug mode
    const std::string mode =
        Debug::getIsDebugLogEnabled() ? "DEBUG" : "STANDARD";
    if (Debug::getIsDebugLogEnabled()) std::cout << Debug::LogType[1] << ": ";
    syncCout << "Current play mode is " << mode << syncEndl;
}


// logs a given rootNode's tree to a file
void Console::logNodeTree(SearchNode& rootNode) noexcept
{
    // Generate the file name as 'name#.extension'
    std::string fileName = "nodetree";
    std::string fileExt = ".txt";
    int treeFileNum = 0;
    std::stringstream fullFileName;
    fullFileName << fileName << treeFileNum << fileExt;

    // Generate a unique file name and then print it
    while(std::ifstream(fullFileName.str().c_str()))
    {
        fullFileName = std::stringstream();
        treeFileNum++;
        fullFileName << fileName << treeFileNum << fileExt;
        // syncCout << fullFileName.str() << syncEndl;
    }
    syncCout << fullFileName.str() << syncEndl;

    // Write to the log
    std::ofstream out(fullFileName.str().c_str());
    writeNodeTree(rootNode,out);
    out.close();

    syncCout << "Done" << syncEndl;
}


// Writes a node tree to a file given a root node
void Console::writeNodeTree(SearchNode& node, std::ofstream& out,
        const int nodeLevel) noexcept
{
    // Align node tree with hypens (-)
    assert(nodeLevel > 0);
    for(int i = 0; i < nodeLevel; i++)
    {
        out << ("-");
    }
    out << ("- ");

    // Make a move?
    out << (Moves::createMoveString(node.move));
    out << (" Eval: ");
    out << (node.eval);
    out << (std::endl);

    // Print lower-level trees
    for(auto nextNode : node.nextNodes)
    {
        writeNodeTree(nextNode, out, nodeLevel + 1);
    }
}

