/**
 * @file debug.cpp
 * @brief Debug class implementation
 * @author Clint B
 * @date 1/24/2019
 */
#include "debug.h"

#include <iostream>

#include "types.h"

/**
 * @brief Labelling for log types
 * Strings for labelling debug logs
 * [0] INFO
 * [1] WARNING
 */
const char* Debug::LogType[] = { "INFO", "WARNING" };

// Set static debug enabled flag
bool Debug::_isDebugLogEnabled = false;

