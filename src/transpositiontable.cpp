/**
 * @file transpositiontable.cpp
 * @brief TranspositionTable class implementation
 * @author Paul P
 * @date 2/21/2019
 */
#include "transpositiontable.h"

#include <iterator>
#include <shared_mutex>

#include "coutsync.h"


// Global hash table of bitboards and more
TranspositionTable TTable((1024)*(1024)*(1024)/sizeof(TTEntry)/2);


// Constructor
/*
TranspositionTable::TranspositionTable(const size_t tableSize)
    : maxSize(tableSize)
{
    hashTable = cuckoohash_map<Bitboard,TTEntry>();
    // syncCout << maxSize << syncEndl;
}
*/


// Looks up a position in the table, if exists
TTEntry TranspositionTable::Lookup(const Position& pos)
{
    TTEntry entry;
    if (hashTable.find(pos.getHash(), entry)) hashHits++;
    return entry;
}


// Stores a given TTEntry in the map
void TranspositionTable::Store(const Position& pos, TTEntry entry)
{
    // Update values for the TTEntry
    entry.timeUpdated = std::chrono::high_resolution_clock::now();
    entry.isValid = true;
    entry.bestMove.type = Hash;

    // See if the hash is valid
    TTEntry hashEntry;
    hashTable.find(pos.getHash(), hashEntry);
    if (hashEntry.isValid)
    {
        // Insert the TTEntry
        if (hashEntry.depth > entry.depth)
        {
            hashEntry.timeUpdated = entry.timeUpdated;
            entry = hashEntry;
        }
        hashTable.insert_or_assign(pos.getHash(), entry);
    }
    else if (size >= maxSize)
    {
        // Replacement algorithm: replace an entry
        // Remove from list
        bool erasedEntry = false;
        {
            while (!erasedEntry)
            {
                // Attempt to dequeue the TTEntry
                AgeType expiration;
                {
                    if (!ageQueue.try_dequeue(expiration)) break;
                }

                // Attempt to erase the element
                TTEntry ttEntry;
                if (hashTable.find(std::get<1>(expiration), ttEntry))
                {
                    if (ttEntry.timeUpdated <= std::get<0>(expiration))
                    {
                        if (hashTable.erase(std::get<1>(expiration)))
                        { erasedEntry = true; }
                    }
                }
            }
        }

        // If removed an element, add in the new one
        if (erasedEntry)
        {
            hashTable.insert_or_assign(pos.getHash(),entry);
        }
    }
    else
    {
        // Perform a simple insert
        size++;
        hashTable.insert_or_assign(pos.getHash(),entry);
    }
    ageQueue.enqueue(std::make_tuple(entry.timeUpdated, pos.getHash()));
}

