/**
 * @file zhash.cpp
 * @brief ZobristHash namespace implementation
 * @author Paul Grahek
 * @date 2/21/2019
 */
#include "zhash.h"

#include <random>

#include "coutsync.h"

// 3-D bitboard
Bitboard ZobristHash::zPieceTable[PieceColorCount][PieceTypeCount + 1][64];
    // = std::array<std::array<Bitboard,12>,64>();

// 1-D color table
Bitboard ZobristHash::zColorTable[PieceColorCount];


// Initialization of ZobristHash
void ZobristHash::init()
{
    // Use a random hasher
    std::mt19937_64 rd(232554);

    // Iterate over all dimensions and store the hash value
    for(unsigned int i = 0; i < PieceColorCount; i++)
    {
        for(unsigned int j = 0; j < PieceTypeCount + 1; j++)
        {
            if (j == NoType)
            {
                for(unsigned int k = 0; k < 64; k++)
                { zPieceTable[i][j][k] = 0; }
            }
            else
            {
                for(unsigned int k = 0; k < 64; k++)
                {
                    Bitboard val = rd();
                    zPieceTable[i][j][k] = val;
                }
            }
        }
    }
    zColorTable[White] = 0;
    zColorTable[Black] = rd();
    // auto table = zPieceTable;
}


// Hashes a given position and retruns the hashed position
Bitboard ZobristHash::hash(const Position& pos)
{
    Bitboard hash = 0;
    for(PieceColor pc = White; pc != PieceColorCount; ++pc)
    {
        for(PieceType pt = Pawn; pt != PieceTypeCount; ++pt)
        {
            for(SquareIndex ind : getSquareIndexesForBitboard(
                        pos.getPiecePositions(pc, pt)))
            {
                hash ^= zPieceTable[pc][pt][ind];
            }
        }
    }
    hash ^= zColorTable[pos.getCurrentMoveColor()];
    return hash;
}

