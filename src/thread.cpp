/**
 * @file thread.cpp
 * @brief TranspositionTable class implementation
 * @author Paul P
 * @date 1/20/2019
 */
#include "thread.h"

#include <chrono>

#include "search.h"


// Declaring the size of the worker pool
WorkerPool SearchPool(4);


// Primary loop for worker to wait for SearchQueries
void Worker::operator()()
{
    SearchQuery task;
    for(;;)
    {
        {
            std::unique_lock<std::mutex> lock(pool.inQueueMutex);

            // Have worker wait
            while (!pool.kill && pool.tasks.empty())
            {
                pool.inQueueNotEmpty.wait(lock);
            }
            if(pool.kill) return;

            // pool.idleWorkers-=1;
            task = pool.tasks.front();
            pool.tasks.pop_front();
        }
        SearchResult retVal = Search::workerSearch(task, *this);
        {
            std::unique_lock<std::mutex> lock(pool.outQueueMutex);
            pool.returnVals.emplace_back(retVal);
        }
        pool.outQueueNotEmpty.notify_one();
        // pool.idleWorkers += 1;
    }
}


// Primary loop for the Mast Worker to wait for Search Requests
void MasterWorker::operator()()
{
    for(;;)
    {
        {
            std::unique_lock<std::mutex> lock(pool.searchMutex);
            pool.go.wait(lock);
            if (pool.kill) return;
            Position posToSearch = pool.rootPos;
            std::vector<Move> movesToSearch = pool.movesToSearch;
            auto rootNode = Search::masterSearch(posToSearch,
                    pool.maxDepth,pool.movesToSearch);
        }
    }
}


// WorkerPool Constructor
WorkerPool::WorkerPool(const size_t threadCount)
    : threadCount(threadCount)
{
    for(size_t i = 0; i < threadCount; i++)
    {
        workers.emplace_back(std::thread(Worker(*this)));
    }
    masterThread = std::thread(MasterWorker(*this));
}


// Worker Destructor
WorkerPool::~WorkerPool()
{
    kill = true;

    // Notify all workers and stop the work
    inQueueNotEmpty.notify_all();
    go.notify_all();
    for (auto& it : workers)
    { it.join(); }
    masterThread.join();
}


// Adds a SearchQuery to in InQueue
void WorkerPool::enqueue(SearchQuery query)
{
    {
        std::unique_lock<std::mutex> lock(inQueueMutex);
        tasks.push_back(query);
    }
    inQueueNotEmpty.notify_one();
}


// Starts a search
void WorkerPool::startSearch(
        const Position& startpos, const std::vector<Move>& movesToSearch)
{
    endSearch = false;
    {
        std::unique_lock<std::mutex> lock(searchMutex);
        this->rootPos = startpos;
        this->movesToSearch = std::move(movesToSearch);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    go.notify_one();
}


// Stops the search
void WorkerPool::stopSearch()
{
    endSearch = true;
}


// Gets the output of the Threadpool
SearchOutput WorkerPool::getOutput()
{
    std::unique_lock<std::mutex> lock(SearchPool.outputMutex);
    return output;
}


// Pops a SearchResults off of the OutQueue
SearchResult WorkerPool::pop_result()
{
    SearchResult retVal;
    {
        std::unique_lock<std::mutex> lock(outQueueMutex);
        while (!kill && returnVals.empty())
        { outQueueNotEmpty.wait(lock); }
        if (kill) return retVal;

        retVal = returnVals.front();
        returnVals.pop_front();
    }
    return retVal;
}

