/**
 * @file move.cpp
 * @brief Move namespace implementation
 * @author Paul Grahek
 * @date 2/06/2019
 */
#include "move.h"

#include "position.h"
#include "types.h"

// Creates a move from a given UCI string
Move Moves::parseMoveString(std::string strMove)
{
    // Convert string to lowercase
    for(int i = 0; i < strMove.size();i++)
    {
        strMove[i] = (char)tolower(strMove[i]);
    }

    // Create a move and load data
    Move move = Move();
    if(strMove.size() < 4) return move;
    for(int i = 0; i < SquareCount;i++)
    {
        // Check to make sure the square is not present twice in the move
        size_t validSquare = strMove.find(SquareIndexStrings[i]);
        if (validSquare != std::string::npos)
        {
            if (validSquare < 2)
            {
                move.startSquare = (SquareIndex)i;
            }
            else
            {
                move.endSquare = (SquareIndex)i;
            }
        }
    }
    // Check for invalid move
    if(move.startSquare == none || move.endSquare == none)
    {
        return move;
    }
    else
    {
        // If it is not another piece, assume it is a pawn.
        move.pieceType = Pawn;
    }

    //
    for (unsigned int i = 0; i < PieceTypeCount; i++)
    {
        // Ensure the piece is valid (i.e. has a name)
        if (!PieceTypeLowercaseShortStrings[i].empty())
        {
            // Find the piece in the UCI move string
            std::string pieceTypeStr = PieceTypeLowercaseShortStrings[i];
            size_t firstInd = strMove.find_first_of(pieceTypeStr);

            // Determine the move type for the piece being queried
            if (firstInd != std::string::npos)
            {
                if (pieceTypeStr == "b")
                {
                    // Move a bishop or promote a pawn.
                    // If bb is found, it is a bishop move
                    if (strMove.find_first_of("bb") == 0)
                    {
                        move.pieceType = Bishop;
                        break;
                    }
                    else if(strMove.find_last_of("b") == strMove.size() - 1)
                    {
                        // Pawn promotion!
                        move.pieceType = Pawn;
                        move.promotionType = Bishop;
                        break;
                    }
                }
                else if (firstInd == 0)
                {
                    // Simply moving the piece; set and exit
                    move.pieceType = (PieceType)i;
                    break;
                }
                else if (firstInd == strMove.size() - 1)
                {
                    //If at back of string, it's a pawn promotion
                    move.pieceType = Pawn;
                    move.promotionType = (PieceType)i;
                    break;
                }
            }
        }
    }

    // If it is a castling move, return as such.
    if(move.pieceType == Pawn &&
        ((move.startSquare == e1 && move.endSquare == g1) ||
         (move.startSquare == e1 && move.endSquare == c1) ||
         (move.startSquare == e8 && move.endSquare == g8) ||
         (move.startSquare == e8 && move.endSquare == c8)))
    {
        move.pieceType = King;
        move.promotionType = NoType;
        return move;
    }
    //Nullmove: 0000
    return move;
}


// Create a string representing the move (UCI compatible)
std::string Moves::createMoveString(const Move& move)
{
    // Generate the string and determine piece type
    std::string moveString;
    if(move.pieceType == Pawn)
    {
        moveString += SquareIndexStrings[move.startSquare];
        moveString += SquareIndexStrings[move.endSquare];
        moveString += PieceTypeShortStrings[move.promotionType];
    }
    else
    {
        // If it is a castling move
        if(move.pieceType != King ||
           !((move.startSquare == e1 && move.endSquare == g1) ||
            (move.startSquare == e1 && move.endSquare == c1) ||
            (move.startSquare == e8 && move.endSquare == g8) ||
            (move.startSquare == e8 && move.endSquare == c8)))
        {
            moveString += PieceTypeShortStrings[move.pieceType];
        }
        moveString += SquareIndexStrings[move.startSquare];
        moveString += SquareIndexStrings[move.endSquare];
    }
    return moveString;
}

