/**
 * @file evaluation.cpp
 * @brief Evaluation namespace implementation
 * @author Paul Grahek
 * @date 1/28/2019
 */
#include "evaluation.h"

#include <atomic>

#include "movegen.h"


// Declaration of historyHeuristic
std::atomic<uint64_t>
    historyHeuristic[PieceColorCount][PieceTypeCount][SquareCount];

// Declaration of the WeightArray
std::array<int, NoType + 1> Evaluation::WeightArray;


// Initiaties any object assoicated with Evaluation
void Evaluation::init()
{
    WeightArray[Pawn]   = PawnWeight   * EvaluationOffset;
    WeightArray[Bishop] = BishopWeight * EvaluationOffset;
    WeightArray[Knight] = KnightWeight * EvaluationOffset;
    WeightArray[Rook]   = RookWeight   * EvaluationOffset;
    WeightArray[Queen]  = QueenWeight  * EvaluationOffset;
    WeightArray[King]   = KingWeight   * EvaluationOffset;
    WeightArray[PieceTypeCount] = 0;
    WeightArray[NoType] = 0;
}


// Calculates the heuristic value of the given position
int Evaluation::evalPosition(const Position& pos)
{
    // Evaluate based on current game state
    switch (pos.getCurrentGameState())
    {
        case Checkmate:
            return SEARCH_MIN;
        case Stalemate:
            return 0;
        case Check:
        case Normal:
        default:
            auto mobWhite = 0, mobBlack = 0;
            mobWhite += MoveGen::getPossibleMoveCountForKnights(pos,White);
            mobBlack += MoveGen::getPossibleMoveCountForKnights(pos,Black);
            mobWhite += MoveGen::getPossibleMoveCountForBishops(pos,White);
            mobBlack += MoveGen::getPossibleMoveCountForBishops(pos,Black);
            mobWhite += MoveGen::getPossibleMoveCountForKings(pos,White);
            mobBlack += MoveGen::getPossibleMoveCountForKings(pos,Black);
            mobWhite += MoveGen::getPossibleMoveCountForPawns(pos,White,none);
            mobBlack += MoveGen::getPossibleMoveCountForPawns(pos,Black,none);
            mobWhite += MoveGen::getPossibleMoveCountForQueens(pos,White);
            mobBlack += MoveGen::getPossibleMoveCountForQueens(pos,Black);
            mobWhite += MoveGen::getPossibleMoveCountForRooks(pos,White);
            mobBlack += MoveGen::getPossibleMoveCountForRooks(pos,Black);
            int mobility = 10 * (mobWhite - mobBlack);
            auto weight_sum =
                WeightArray[King] * (
                    numOnes64(pos.getPiecePositions(White,King)) -
                    numOnes64(pos.getPiecePositions(Black,King))) +
                WeightArray[Queen] * (
                    numOnes64(pos.getPiecePositions(White,Queen)) -
                    numOnes64(pos.getPiecePositions(Black,Queen))) +
                WeightArray[Rook] * (
                    numOnes64(pos.getPiecePositions(White,Rook)) -
                    numOnes64(pos.getPiecePositions(Black,Rook))) +
                WeightArray[Bishop] * (
                    numOnes64(pos.getPiecePositions(White,Bishop)) -
                    numOnes64(pos.getPiecePositions(Black,Bishop))) +
                WeightArray[Knight] * (
                    numOnes64(pos.getPiecePositions(White,Knight)) -
                    numOnes64(pos.getPiecePositions(Black,Knight))) +
                WeightArray[Pawn] * (
                    numOnes64(pos.getPiecePositions(White,Pawn)) -
                    numOnes64(pos.getPiecePositions(Black,Pawn)));
            return PieceColorWeight[pos.getCurrentMoveColor()] *
                (weight_sum + mobility);
    }
}


// Determines if exchanges on a square will have gain or loss
/*
double Evaluation::SEE(const Position& pos, SquareIndex sq)
{
    double value = 0;
    std::tuple<PieceType, SquareIndex> smallestPiece =
        pos.getSmallestAttacker(sq);

    if (std::get<0>(smallestPiece) != NoType)
    {
        Move mv;
        mv.pieceType = std::get<0>(smallestPiece);
        mv.startSquare = std::get<1>(smallestPiece);
        mv.endSquare = sq;
        pos.move(mv);
        value = std::max(0.0,WeightArray[mv.pieceType]-SEE(pos,sq));
    }
    return value;
}
*/

