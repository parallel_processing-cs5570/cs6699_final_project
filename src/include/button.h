/**
 * @file button.h
 * @brief Declaration file for the button class
 * @author Bryan T
 * @date 4/19/2019
 */
#pragma once

#include <string>

#include "SDL2/SDL.h"

/**
 * @class Button
 * @brief General structure of a button
 * @author Bryant Foster
 *
 * Contains data and methods that are the same among buttons.
 */
class Button
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief Button label
    std::string _content = "";

    //! @brief X position of top-left corner
    double _topLeftX = 0;

    //! @brief Y position of top-left corner
    double _topLeftY = 0;

    //! @brief Height of the button
    double _height = 0;

    //! @brief Width of the button
    double _width = 0;

    //! @brief X position of text (at front)
    double _textX = 0;

    //! @brief Y position of the text
    double _textY = 0;

    //! @brief Width of the text
    double _textWidth = 0;

    //! @brief Scale factor of the screen
    float _screenScale = 2;

    //! @brief Flags if the button was clicked
    bool _clicked = false;

    //! @brief Flags if the button is disabled
    bool _disabled = false;

    //! @brief Flags if the button is dependent on the current GUI state
    bool _guiStateDependent = false;

    //! @brief Counter for the number of times a button was clicked
    int _clickFrameCount = 8;


  public:
    // >>> METHODS

    /**
     * @brief Default constructor
     * @author Bryant Foster
     */
    Button() = default;


    /**
     * @brief Overloaded constructor for the Button struct.
     * @param x  The x coordinate of the top left side
     * @param tx The x coordinate of the top left side of the text
     * @param y  The y coordinate of the top left side
     * @param ty The y coordinate of the top left side of the text
     * @param h  The height
     * @param w  The width
     * @param tw The width of the text
     * @param c  The text to go over the button
     * @param sc The current screen scale
     * @author Bryant Foster
     */
    Button(const double x, const double tx, const double y,  const double ty,
           const double h, const double w,  const double tw,
           const std::string& c, const float sc);


    /**
     * @brief Getter for the content.
     * @return The button's content
     * @author Bryant Foster
     */
    std::string getContent() const noexcept
    { return _content; }


    /**
     * @brief Setter for the content.
     * @param value Value to set the variable to
     * @author Bryant Foster
     */
    void setContent(const std::string& value) noexcept
    { _content = value; }


    /**
     * @brief Getter for the x coordinate.
     * @return The x coordinate
     * @author Bryant Foster
     */
    double getX() const noexcept
    { return _topLeftX; }

    /**
     * @brief Getter for the y coordinate.
     * @return The y coordinate
     * @author Bryant Foster
     */
    double getY() const noexcept
    { return _topLeftY; }

    /**
     * @brief Getter for the width.
     * @return The button's width
     * @author Bryant Foster
     * @todo Change name of \c getWidth
     */
    double getW() const noexcept
    { return _width; }

    /**
     * @brief Getter for the height.
     * @return The button's height
     * @author Bryant Foster
     * @todo Change name of \c getHeigth
     */
    double getH() const noexcept
    { return _height; }

    /**
     * @brief Getter for the text x coordinate.
     * @return The button's text x coordinate
     * @author Bryant Foster
     * @todo Change name of \c getTextX
     */
    double getTX() const noexcept
    { return _textX; }

    /**
     * @brief Getter for the text y coordinate.
     * @return The button's text y coordinate
     * @author Bryant Foster
     * @todo Change name of \c getTextY
     */
    double getTY() const noexcept
    { return _textY; }

    /**
     * @brief Getter for the text width.
     * @return The button's text width
     * @author Bryant Foster
     * @todo Change name of \c getTextWidth
     */
    double getTW() const noexcept
    { return _textWidth; }

    /**
     * @brief Getter for the disabled flag.
     * @return The button's disabled status
     * @author Bryant Foster
     */
    bool getDisabled() const noexcept
    { return _disabled; }

    /**
     * @brief Setter for the disabled flag.
     * @param value Value to set the variable to
     * @author Bryant Foster
     */
    void setDisabled(const bool value) noexcept
    { _disabled = value; }

    /**
     * @brief Getter for the gui state dependency flag.
     * @return The button's gui state dependency status
     * @author Bryant Foster
     */
    bool getGuiStateDependent() const noexcept
    { return _guiStateDependent; }

    /**
     * @brief Setter for the Gui State Dependency.
     * @param value Value to set the variable to
     * @author Bryant Foster
     */
    void setGuiStateDependent(const bool value) noexcept
    { _guiStateDependent = value; }

    /**
     * @brief Returns true/false if the button was clicked
     * @param mouseX The mouse's x coordinate
     * @param mouseY The mouse's y coordinate
     * @return True/False it was clicked or not
     * @author Bryant Foster
     */
    bool wasClicked(const double mouseX, const double mouseY) const noexcept;

    /**
     * @brief Updates the clickFrameCount and clicked as needed.
     * @author Bryant Foster
     */
    void updateClickData() noexcept;

    /**
     * @brief Returns the color of the button
     * @return The button's color
     * @author Bryant Foster
     *
     * Returns the color of the button based on if it is disabled or not,
     * and if it is clicked or not.
     */
    SDL_Color getColor() const noexcept;

};

