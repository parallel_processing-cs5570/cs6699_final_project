/**
 * @file cli.h
 * @brief Declaration file for the \c cli class
 * @author Paul P
 * @date 1/17/2019
 */
#pragma once

/**
 * @brief Some non-UCI chess interface
 * @author Paul P
 * \todo Implement for interface usage
 */
class cli
{
};

