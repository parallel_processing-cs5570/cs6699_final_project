/**
 * @file debug.h
 * @brief Declaration of the \c debug class
 * @author Paul P, Clint B
 * @date 1/17/2019
 */
#pragma once

#include <iostream>

#include "types.h"

/**
 * @class Debug
 * @brief Generates labels for Debug mode
 * @author Paul P, Clint B
 */
class Debug
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief Flags if debug logging is enabled
    static bool _isDebugLogEnabled;

  public:
    /**
     * @brief String of log types
     * Strings for labelling debug logs
     * [0] INFO
     * [1] WARNING
     */
    static const char* LogType[];


  public:
    // >>> METHODS

    //! @brief Sets if debug logging is enabled
    static void setIsDebugLogEnabled(const bool value)
    { _isDebugLogEnabled = value; }

    //! @brief Returns flag indicating if debug logging is enabled
    static bool getIsDebugLogEnabled()
    { return _isDebugLogEnabled; }

};

