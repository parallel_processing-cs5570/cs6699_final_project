/**
 * @file debugging.h
 * @brief Declares a debugging method
 * @author Paul G
 * @date 4/10/2019
 */
#pragma once

#include "position.h"
#include "types.h"

//! @brief Tests the undo of a move
void testUndoMove()
{
    // Create some objects
    Position pos;
    Position nextPos(pos);
    Move mv;

    // Give some starting points
    mv.startSquare = g1;
    mv.endSquare = h3;
    mv.pieceType = Knight;
    Move mv2;
    mv2.startSquare = g7;
    mv2.endSquare = g5;
    mv2.pieceType = Pawn;
    Move mv3;
    mv3.startSquare = h3;
    mv3.endSquare = g5;
    mv3.pieceType = Knight;

    // Move
    nextPos.move(mv);
    nextPos.move(mv2);
    nextPos.move(mv3);
    pos.move(mv);
    pos.move(mv2);
//    pos.undoLastMove();
//    pos.undoLastMove();
//    nextPos.undoLastMove();
//    nextPos.undoLastMove();
    nextPos.undoLastMove();

    // Make some assertions based on the moves
    assert(nextPos.getHash() == pos.getHash());
    assert(nextPos.getPieceBoards() == pos.getPieceBoards());
    assert(nextPos.getColorPositions() == pos.getColorPositions());
}

