/**
 * @file evaluation.h
 * @brief Declaration of the \c Evaluation namespace
 * @author Paul Grahek
 * @date 1/28/2019
 */
#pragma once

#include <atomic>

#include "position.h"

/**
 * @namespace Evaluation
 * @brief  Contains weights and methods for evaluation pieces in the game
 * @author Paul Grahek
 */
namespace Evaluation
{

    //@{
    //! @brief Weights (importances) of each piece
    constexpr int KingWeight = 10000;
    constexpr int QueenWeight = 12;
    constexpr int RookWeight = 6;
    constexpr int BishopWeight =4;
    constexpr int KnightWeight = 4;
    constexpr int PawnWeight = 1;
    //@}

    //! @brief Offsets the evaluation
    constexpr int EvaluationOffset = 100;

    //! @brief Declaration of an array of weights
    extern std::array<int, NoType + 1> WeightArray;

    //! @brief Declaration of some history
    extern std::atomic<uint64_t>
        historyHeuristic[PieceColorCount][PieceTypeCount][SquareCount];


    /**
     * @brief Initiates the \c WeightArray
     * Initiates any object associated with Evaluation
     * @author Paul Grahek
     */
    void init();


    /**
      * @brief Calculates the heuristic value of the given position
      * @param pos
      * @return
      * @author Porter Glines 2/10/19
      */
    int evalPosition(const Position& pos);


    /**
     * @brief Determines if exchanges on a square will have gain or loss
     * Static Exchange Evaluation: Determines if exchanges on a square
     * will result in a net gain or loss in material
     * @param sq
     * @return
     * @author Paul Grahek
     */
    // double SEE(const Position& pos, SquareIndex sq);

};

