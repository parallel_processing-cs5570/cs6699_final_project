/**
 * @file move.h
 * @brief Declaration of the \c Moves namespace
 * @author Paul Grahek
 * @date 2/06/2019
 */
#pragma once

#include <string>

#include "types.h"

/**
 * @namespace Moves
 * @brief Converts a string to a Move and vice versa
 * @author Paul Grahek
 * @date 2/06/2019
 */
namespace Moves
{

    /**
     * @brief Creates a move from a given UCI string
     * @param strMove
     * @return
     * @author Paul Grahek
     */
    Move parseMoveString(std::string strMove);


    /**
     * @brief Creates a string representation of a move (UCI compatible)
     * @param move
     * @return
     * @author Paul Grahek
     */
    std::string createMoveString(const Move& move);
};

