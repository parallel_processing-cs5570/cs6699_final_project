/**
 * @file option.h
 * @brief Declaration of the \c Option class
 * @author Paul P
 * @date 1/19/2019
 */
#pragma once

#include <string>
#include <typeinfo>

/**
 * @brief Provides program options
 * @tparam T
 * @author Clinton Brown
 */
template <class T>
class Option
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief The default value
    T defaultVal;

    //! @brief The current value
    T currVal;


  public:
    // >>> METHODS

    //! @brief Constructor
    Option() = default;

    //! @brief Constructor
    explicit Option(const T val)
        : defaultVal(val)
        , currVal(val)
    { /* * */ }

    /**
     * @brief Resets the option to its default value
     * @author Clinton Brown
     */
    void resetToDefault()
    { currVal = defaultVal; }

    /**
     * @brief Returns the option type
     * @return
     * @author Clinton Brown
     *
     * Returns the type of the current value stored in this option
     * @note This is sometimes not in a user friendly format
     */
    std::string getOptionType()
    { return typeid(this->currVal).name(); }

};

template class Option<bool>;
template class Option<int>;

