/**
 * @file enginetools.h
 * @brief Declaration of the \c EngineTools class
 * @author Paul P
 * @date 1/19/2019
 */
#pragma once

#include <vector>
#include <string>

#include "types.h"


/**
 * @class EngineTools
 * @brief Defines various tools for the chess engine
 * @author Paul P
 * @date 1/19/2019
 */
class EngineTools
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief Name of the chess engine
    const static std::string EngineName;

    //! @brief List of authors, in a comma-separated list
    const static std::string AuthorList;


  public:
    // >>> METHODS

    /**
     * @brief Concatenates a list of strings, separated by a space ( )
     * Returns a vector of strings as a single string with space-separated
     * entries
     * @return
     * @author Paul Grahek
     * @todo Rename to \c concatenateStrings
     */
    static std::string vectorToString(const std::vector<std::string>&);


    /**
     * @brief Returns an all lowercase string
     * @return
     * @author Paul Grahek
     */
    static std::string stringToLower(const std::string&);


    /**
     * @brief Returns the string equivalent of a boolean
     * @return
     * @author Paul Grahek
     */
    static std::string boolToString(const bool val)
    { return val ? "true" : "false"; }


    /**
     * @brief Returns the engine info string for UCI
     * @return
     * @author Paul Grahek
     */
    static std::string EngineInfo(EngineMode);


    /**
     * @brief Returns the name of the Chess Engine
     * @return
     * @author Paul Grahek
     */
    static std::string getEngineName()
    { return EngineName; }


    /**
     * @brief Returns the list of Authors of the engine
     * @return
     * @author Paul Grahek
     */
    static std::string getAuthorList()
    { return AuthorList; }

};

