/**
 * @file console.h
 * @brief Declaration of the \c Console class
 * @author Clint B
 * @date 1/24/2019
 */
#pragma once

#include "types.h"
#include "position.h"

/**
 * @class Console
 * @brief The GUI interface for playing the game
 * @author Clint B
 * @date 1/24/19
 */
class Console
{
  private:
    // >>> IMPLEMENTATION DATA


  public:
    // >>> METHODS

    /**
     * @brief Prints the contents of a single bitboard
     * @param bb bitboard to print
     * @author Clinton Brown
     * @return
     */
    static void printBitboard(Bitboard bb) noexcept;


    /**
     * @brief Prints the combined content of bitboards for display
     *
     * Prints all the pieces on the pieces on the bitboard by cross
     * referencing the position bitboards.
     *
     * @author Clinton Brown
     * @return
     */
    static void printCombinedBitboards(std::shared_ptr<Position>) noexcept;


    /**
     * @brief Prints the start message to show when the program launches
     * @author Clinton Brown
     * @return
     */
    static void printStartMsg() noexcept;


    /**
     * @brief logs a given rootNode's tree to a file.
     * @param rootNode
     * @author Paul Grahek
     */
    static void logNodeTree(SearchNode& rootNode) noexcept;

    /**
     * @brief Recursively writes a node tree to a file given a root node.
     * @param node Node to Print
     * @param out Out Stream
     * @param nodeLevel recursive level
     * @author Paul Grahek
     */
    static void writeNodeTree(SearchNode& node, std::ofstream& out,
            const int nodeLevel=0) noexcept;

};

