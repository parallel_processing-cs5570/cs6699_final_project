/**
 * @file graphics.h
 * @brief Declaration of the \c Graphics class
 * @author Clint B
 * @date 2/24/2019
 */
#pragma once

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

#include "position.h"
#include "types.h"

/**
 * @class Graphics
 * @brief Container for all graphics of the chess game
 * @author Clint B
 * @date 2/24/2019
 *
 * Contains the graphics for the board tiles and pieces.
 */
class Graphics
{
  public:
    // >>> IMPLEMENTATION DATA

    //! @brief Width of a tile
    static constexpr int TILE_WIDTH = 32;

    //! @brief Height of a tile
    static constexpr int TILE_HEIGHT = 32;

  private:
    /**@brief GUI image containing board tiles
     * @author Clinton Brown
     */
    SDL_Texture* _boardImage;

    /**@brief Image containing sprites for the chess pieces
     * @author Clinton Brown
     */
    SDL_Texture* _piecesImage;

    /**
     * @brief Image containing the alternate sprites for the chess pieces.
     * @author Bryant Foster
     */
    SDL_Texture* _altPiecesImage;

    /**@brief GUI display tile for white board spaces
     * @author Clinton Brown
     */
    SDL_Texture* _whiteBoardTile;

    /**@brief GUI display tile for black board spaces
     * @author Clinton Brown
     */
    SDL_Texture* _blackBoardTile;

    /**
     * @brief Individual border tiles
     * @author Bryant Foster
     */
    SDL_Texture* _boardBorderTopLeftTile;
    SDL_Texture* _boardBorderTopRightTile;
    SDL_Texture* _boardBorderTopMiddleTile;
    SDL_Texture* _boardBorderLeftMiddleTile;
    SDL_Texture* _boardBorderRightMiddleTile;
    SDL_Texture* _boardBorderBottomLeftTile;
    SDL_Texture* _boardBorderBottomRightTile;
    SDL_Texture* _boardBorderBottomMiddleTile;

    // Individual sprites for pieces
    // author Clinton Brown
    SDL_Texture* _blackPawnSprite;
    SDL_Texture* _whitePawnSprite;
    SDL_Texture* _blackRookSprite;
    SDL_Texture* _whiteRookSprite;
    SDL_Texture* _blackKnightSprite;
    SDL_Texture* _whiteKnightSprite;
    SDL_Texture* _blackBishopSprite;
    SDL_Texture* _whiteBishopSprite;
    SDL_Texture* _blackQueenSprite;
    SDL_Texture* _whiteQueenSprite;
    SDL_Texture* _blackKingSprite;
    SDL_Texture* _whiteKingSprite;

    // Individual sprites for the alternate pieces
    // author Bryant Foster
    SDL_Texture* _altBlackPawnSprite;
    SDL_Texture* _altWhitePawnSprite;
    SDL_Texture* _altBlackRookSprite;
    SDL_Texture* _altWhiteRookSprite;
    SDL_Texture* _altBlackKnightSprite;
    SDL_Texture* _altWhiteKnightSprite;
    SDL_Texture* _altBlackBishopSprite;
    SDL_Texture* _altWhiteBishopSprite;
    SDL_Texture* _altBlackQueenSprite;
    SDL_Texture* _altWhiteQueenSprite;
    SDL_Texture* _altBlackKingSprite;
    SDL_Texture* _altWhiteKingSprite;

    /**@brief Font for GUI display
     * @author Clinton Brown
     */
    TTF_Font* _font;


  public:
    // >>> METHODS

    //! @brief Simple constructor from the SDL_Renderer
    explicit Graphics(SDL_Renderer*);


    //! @brief Destructor
    ~Graphics();


    /**
     * @brief Translates the bitboard locations onto a 2-D display in the UI
     * @author Clinton Brown
     * @param Position object with bitboard position information
     * @param char[8][8] array to put translated positions into
     *
     * Translates bitboard locations onto a 2D char array for display in the UI
     *
     */
    static void createDisplayBitboard(std::shared_ptr<Position>, char[8][8]);


    /**
     * @brief Gets the white checkerboard tile texture
     * @author Clinton Brown
     * @return The white checkerboard tile texture
     */
    SDL_Texture* getWhiteBoardTile() noexcept
    { return _whiteBoardTile; }


    /**
     * @brief Gets the black checkerboard tile texture
     * @author Clinton Brown
     * @return The black checkerboard tile texture
     */
    SDL_Texture* getBlackBoardTile() noexcept
    { return _blackBoardTile; }


    //@{
    //! @brief Getters for the border tiles.
    //! @author Bryant Foster
    SDL_Texture* getBorderTileTopLeft() noexcept
    { return _boardBorderTopLeftTile; }

    SDL_Texture* getBorderTileTopMiddle() noexcept
    { return _boardBorderTopMiddleTile; }

    SDL_Texture* getBorderTileTopRight() noexcept
    { return _boardBorderTopRightTile; }

    SDL_Texture* getBorderTileLeftMiddle() noexcept
    { return _boardBorderLeftMiddleTile; }

    SDL_Texture* getBorderTileRightMiddle() noexcept
    { return _boardBorderRightMiddleTile; }

    SDL_Texture* getBorderTileBottomLeft() noexcept
    { return _boardBorderBottomLeftTile; }

    SDL_Texture* getBorderTileBottomMiddle() noexcept
    { return _boardBorderBottomMiddleTile; }

    SDL_Texture* getBorderTileBottomRight() noexcept
    { return _boardBorderBottomRightTile; }
    //@}


    //@{
    //! @brief  Getters for individual sprites for pieces
    //! @author Clinton Brown
    SDL_Texture* getBlackPawnSprite() noexcept
    { return _blackPawnSprite; }

    SDL_Texture* getWhitePawnSprite() noexcept
    { return _whitePawnSprite; }

    SDL_Texture* getBlackRookSprite() noexcept
    { return _blackRookSprite; }

    SDL_Texture* getWhiteRookSprite() noexcept
    { return _whiteRookSprite; }

    SDL_Texture* getBlackKnightSprite() noexcept
    { return _blackKnightSprite; }

    SDL_Texture* getWhiteKnightSprite() noexcept
    { return _whiteKnightSprite; }

    SDL_Texture* getBlackBishopSprite() noexcept
    { return _blackBishopSprite; }

    SDL_Texture* getWhiteBishopSprite() noexcept
    { return _whiteBishopSprite; }

    SDL_Texture* getBlackQueenSprite() noexcept
    { return _blackQueenSprite; }

    SDL_Texture* getWhiteQueenSprite() noexcept
    { return _whiteQueenSprite; }

    SDL_Texture* getBlackKingSprite() noexcept
    { return _blackKingSprite; }

    SDL_Texture* getWhiteKingSprite() noexcept
    { return _whiteKingSprite; }
    //@}

    //@{
    //! @brief Getters for individual alternate sprites for pieces
    //! @author Bryant Foster
    SDL_Texture* getAlternateBlackPawnSprite() noexcept
    { return _altBlackPawnSprite; }

    SDL_Texture* getAlternateWhitePawnSprite() noexcept
    { return _altWhitePawnSprite; }

    SDL_Texture* getAlternateBlackRookSprite() noexcept
    { return _altBlackRookSprite; }

    SDL_Texture* getAlternateWhiteRookSprite() noexcept
    { return _altWhiteRookSprite; }

    SDL_Texture* getAlternateBlackKnightSprite() noexcept
    { return _altBlackKnightSprite; }

    SDL_Texture* getAlternateWhiteKnightSprite() noexcept
    { return _altWhiteKnightSprite; }

    SDL_Texture* getAlternateBlackBishopSprite() noexcept
    { return _altBlackBishopSprite; }

    SDL_Texture* getAlternateWhiteBishopSprite() noexcept
    { return _altWhiteBishopSprite; }

    SDL_Texture* getAlternateBlackQueenSprite() noexcept
    { return _altBlackQueenSprite; }

    SDL_Texture* getAlternateWhiteQueenSprite() noexcept
    { return _altWhiteQueenSprite; }

    SDL_Texture* getAlternateBlackKingSprite() noexcept
    { return _altBlackKingSprite; }

    SDL_Texture* getAlternateWhiteKingSprite() noexcept
    { return _altWhiteKingSprite; }
    //@}

    // Getters for text textures
    // author Clinton Brown
    SDL_Texture* getMousePositionTexture();

    /**@brief Get font for GUI display
     * @author Clinton Brown
     */
     TTF_Font* getFont() noexcept
     { return _font; }

  private:

     /**
     * @brief Translates bitboard locations onto a 2D char array for display in the UI
     * @author Clinton Brown
     * @param Bitboard bitboard to translate
     * @param char[8][8] array to put translated positions into
     * @param char character to replace the bitboard 1s with
     */
    static void bitboardTo2DCharArray(Bitboard, char[8][8], char);

    /**
     * @brief Gets a tile from an image with multiple tiles
     * @author Clinton Brown
     * @param SDL_Rect A rectangle representing a portion of the image
     * @param SDL_Renderer The renderer for the SDL window
     * @param SDL_Texture The source image for the tile to be taken from
     */
    SDL_Texture* getTileFromTexture(SDL_Rect, SDL_Renderer*, SDL_Texture*);

};
