/**
 * @file cli.h
 * @brief Declaration of the ZobristHash namespace
 * @author Paul Grahek
 * @date 2/21/2019
 */
#pragma once

#include <array>

#include "types.h"
#include "position.h"

/**
 * @namespace ZobristHash
 * @brief Contains hashing tool(s) for Bitboards, etc.
 * @author Paul Grahek
 */
namespace ZobristHash
{

    /**
     * @brief 3-D bitboard
     * @author Paul Grahek
     */
    extern Bitboard
        zPieceTable[PieceColorCount][PieceTypeCount + 1][SquareCount];

    /**
     * @brief 1-D color table
     * @author Paul Grahek
     */
    extern Bitboard zColorTable[PieceColorCount];

    /**
     * @brief Initializes the Hash table
     * @author Paul Grahek
     */
    void init();

    /**
     * @brief Hashes a given position and returns the hashed position
     * @param pos
     * @return
     */
    Bitboard hash(const Position& pos);
};

