/**
 * @file thread.h
 * @brief Declaration of various threading classes
 * @author Paul P
 * @date 1/20/2019
 */
#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <queue>
#include <thread>
#include <vector>

#include "position.h"
#include "thread_win32.h"


// Prototype some classes
class Worker;
class WorkerPool;


/**
 * @class Worker
 * @brief Processes search queries sent to it
 *
 * A Worker processes search queries sent to it from the Master Worker.
 * Object is aware of its parent pool for synchronization purposes.
 *
 * @author Paul Grahek 2/4/19
 */
class Worker
{
  public:
    // >>> IMPLEMENTATION DATA

    //! @brief The threadpool owner
    WorkerPool& pool;


  public:
    // >>> METHODS

    /**
     * @brief Instantiates Worker with link to its owner Threadpool
     * @param p
     * @author Paul Grahek
     */
    Worker(WorkerPool& p)
        : pool(p)
    { /* * */ }


    /**
     * @brief Primary loop for worker to wait for SearchQueries
     * @author Paul Grahek
     */
    virtual void operator()();

};


/**
 * @class MasterWorker
 * @brief Takes search queries from the command interface and performs a search
 *
 * A Master Thread takes in search queries from the command interface and
 * performs the tree search.
 *
 * Delegates other branch searching to Worker Threads
 *
 * @author Paul Grahek 2/4/19
 */
class MasterWorker : public Worker
{
  private:

  public:

    /**
     * @brief Instiantiates MasterWorker with link to its owner Threadpool
     * @param p
     * @author Paul Grahek
     */
    MasterWorker(WorkerPool& p)
        : Worker(p)
    { /* * */ }


    /**
     * @brief Primary loop for Master Worker to wait for Search Requests
     * @author Paul Grahek
     */
    void operator()() override;

};


/**
 * @class WorkerPool
 * @brief Processes search queries and controls search threads
 *
 * A Worker Pool processes search queries and controls the threads for
 * the search.
 *
 * @author Paul Grahek 2/4/19
 */
class WorkerPool
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief
    std::atomic<int> currDepth{1};

    //! @brief
    std::atomic<double> outputEval{0};

    //! @brief
    std::atomic<int>  threadCount {8};

    //! @brief
    std::atomic<unsigned long> nodesSearched {0};

    //! @brief
    std::atomic_bool kill = false;

    //! @brief
    std::atomic_bool endSearch = false;

    //! @brief
    std::atomic_bool betaCutoff = false;


    //! @brief
    friend class Worker;

    //! @brief
    friend class MasterWorker;

    //! @brief
    friend class Search;

    //! @brief
    std::vector<std::thread> workers;

    //! @brief
    std::thread masterThread;


    //! @brief
    std::deque<SearchQuery> tasks;

    //! @brief
    std::deque<SearchResult> returnVals;


    //! @brief
    std::mutex inQueueMutex;

    //! @brief
    std::condition_variable inQueueNotEmpty;


    //! @brief
    std::mutex outQueueMutex;

    //! @brief
    std::condition_variable outQueueNotEmpty;


    //! @brief
    std::mutex searchMutex;

    //! @brief
    std::condition_variable go;

    //! @brief
    std::vector<Move> movesToSearch;


    //! @brief
    std::mutex outputMutex;

    //! @brief
    SearchOutput output;


    //! @brief
    Position rootPos;// = Position("4k3/8/8/8/8/8/8/R3K2R w KQkq - 0 1");

    //! @brief
    SearchNode rootNode;

  public:

    //! @brief
    std::atomic_bool done;

    //! @brief
    std::atomic<int> maxDepth{10};


  public:

    /**
     * @brief Specialized constructor
     */
    WorkerPool(const size_t);


    /**
     * @brief Specialization destructor
     */
    ~WorkerPool();


    /**
     * @brief Adds a SearchQuery to in InQueue
     * @param query
     * @author Paul Grahek
     */
    void enqueue(SearchQuery query);


    /**
     * @brief Starts the search for the Threadpool
     * @param startpos
     * @param movesToSearch
     * @author Paul Grahek
     */
    void startSearch(
            const Position& startpos,
            const std::vector<Move>& movesToSearch);


    /**
     * @brief Stops the search for the Threadpool
     * @author Paul Grahek
     */
    void stopSearch();


    /**
     * @brief Gets the Output of the Threadpool
     * @author Paul Grahek
     */
    SearchOutput getOutput();


    /**
     * @brief pops a SearchResult off of the OutQueue
     * @author Paul Grahek
     */
    SearchResult pop_result();

};

//! @brief Worker pool
extern WorkerPool SearchPool;

