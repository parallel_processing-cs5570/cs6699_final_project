/**
 * @file coutsync.h
 * @brief Declaration of the \c CoutSync enumerator
 * @author Clint B
 * @date 1/27/2019
 */
#pragma once

#include <iosfwd>
#include <iostream>
#include <mutex>

/**
 * @brief Enumeration for controlling the synchronous output mutex
 * @author Clint B
 */
enum CoutSync
{
    Lock,
    Unlock
};


/**
 * @brief Overloaded operator for managing synchronous output
 * @param os output stream
 * @param sc Sync lock action
 * @return
 * @author Clint B
 */
inline std::ostream& operator<<(std::ostream& os, const CoutSync sc)
{
    // Create a single mutex
    static std::mutex m;

    // Lock/unlock the single mutex
    switch(sc)
    {
        default:
        case(CoutSync::Lock):
            m.lock();
            break;
        case(CoutSync::Unlock):
            m.unlock();
            break;
    }
    return os;
}

/**
 * @def syncStream
 * @brief The stream used for synchronized output
 */
#define syncStream std::cout


/**
 * @def syncCout
 * @brief Opens a lock of a message to \c std::cout
 */
#define syncCout syncStream << Lock


/**
 * @def syncEndl
 * @brief Closes the lock of a message to \c std::cout
 */
#define syncEndl "\n" << Unlock


/**
 * @def syncCout
 * @brief Closes the lock of a message..?
 */
#define syncEndIn "" << Unlock

