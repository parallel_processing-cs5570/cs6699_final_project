/**
 * @file transpositiontable.h
 * @brief Declaration of the \c TranspositionTable class
 * @author Paul P
 * @date 2/21/2019
 */
#pragma once

#include <atomic>
#include <map>
#include <queue>
#include <shared_mutex>

#include "cuckoo/cuckoohash_map.hh"
#include "moodycamel/concurrentqueue.h"

#include "position.h"
#include "thread_win32.h"
#include "types.h"

/**
 * @struct TTEntry
 * @brief Data entry in the transposition table
 * @author Paul P
 */
struct TTEntry
{
    //! @brief Depth in the queue
    int depth;

    //! @brief
    TTFlag flag;

    //! @brief Value of the entry
    int value;

    //! @brief Best move associated with the entry
    Move bestMove;

    //! @brief Flags if the entry is valid or not
    bool isValid = false;

    //! @brief Time the entry was updated/modified
    std::chrono::high_resolution_clock::time_point timeUpdated;
};


//! @brief Hash map alias
typedef cuckoohash_map<Bitboard,TTEntry> ConcurrentMap;


//! @brief Represents an age
typedef std::tuple<std::chrono::high_resolution_clock::time_point, Bitboard>
    AgeType;


/**
 * @class TranspositionTable
 * @brief Hash table container
 * @author Paul P
 */
class TranspositionTable
{
  private:
    // >>> IMPLEMENTATION DATA

    //! @brief Hash table with all \c TTEntry's
    ConcurrentMap hashTable{};

    //! @brief Queue of \c AgeType and hash elements
    moodycamel::ConcurrentQueue<AgeType> ageQueue{};

    //! @brief
    std::atomic<size_t> maxSize {0};

    //! @brief
    std::atomic<long> hashHits {0};

    //! @brief
    std::atomic<size_t> size {0};

    //! @brief
    friend class Search;


public:

    /**
     * @brief Simple constructor
     * @param tableSize
     * @author Paul Grahek
     */
    TranspositionTable(const size_t tableSize)
        : maxSize(tableSize)
    { /* * */ }


    /**
     * @brief look up a position to see if there is an entry for it in the table
     * @param pos
     * @return
     * @author Paul Grahek
     */
    TTEntry Lookup(const Position& pos);


    /**
     * @brief Stores a given TTEntry in the map
     * @param pos
     * @param entry
     * @author Paul Grahek
     */
    void Store(const Position& pos, TTEntry entry);

};

extern TranspositionTable TTable;

