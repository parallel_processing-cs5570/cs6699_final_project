/**
 * @file uci.cpp
 * @brief UCI namespace implementation
 * @author Paul P
 * @date 1/17/2019
 */
#include "uci.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>

#include "console.h"
#include "coutsync.h"
#include "debug.h"
#include "enginetools.h"
#include "position.h"
#include "window.h"


// Initializes the UCI
void UCI::init()
{ /* * */ }


// Primary interface loop for the UCI
void UCI::loop(int argc, char **argv)
{
    // Have the engine use the UCI interface
    uci();

    // Loop over all input until stopping
    bool quit = false;
    do
    {
        // Get some input
        std::string input;
        getline(std::cin, input);
        if (std::cin.eof()) break;

        // Obtain token list from the input
        std::vector<std::string> tokens = getValidInputTokens(input);
        std::string cmd = (tokens.size() == 0 ? "" : tokens[0]);
        auto argIt = (tokens.size() == 1 ? tokens.end() : tokens.begin() + 1);
        std::vector<std::string> cmdArgs(tokens.end(), tokens.end());

        // For debugging: print token
        /*
        for (auto& it : tokens)
        {
            std::cout << it << " ";
        }
        std::cout << std::endl;
        */

        // Perform action based on UCI command
        if (cmd == "uci") uci();
        else if (cmd == "debug") debug(cmdArgs);
        else if (cmd == "isready") isready();
        else if (cmd == "setoption") setoption(cmdArgs);
        // else if (cmd == "register") registerEngine(cmdArgs);
        else if (cmd == "ucinewgame") ucinewgame();
        else if (cmd == "Position") position(cmdArgs);
        else if (cmd == "go")  go(cmdArgs);
        else if (cmd == "kill") stop();
        else if (cmd == "ponderhit") ponderhit();
        else if (cmd == "quit") quit = true;
        else
        {
            std::cout << "Unknown Command: " << input << std::endl;
        }
    } while (!quit);
}


// Parses input for individual tokens
std::vector<std::string> UCI::getValidInputTokens(const std::string& input)
{
    const std::vector<std::string> validCommands = {
            "uci", "debug", "isready", "setoption", "ucinewgame",
            "Position", "go", "kill", "ponderhit", "quit"};

    // Create a buffer and copy to the tokens
    std::istringstream buffer(input);
    std::vector<std::string> tokens;
    std::copy(std::istream_iterator<std::string>(buffer),
              std::istream_iterator<std::string>(),
              std::back_inserter(tokens));

    // Remove invalid tokens are valid
    for (auto it = tokens.begin(); it != tokens.end(); )
    {
        if (std::find(validCommands.begin(), validCommands.end(), *it)
                != validCommands.end())
        { break; }
        else { it = tokens.erase(it); }
    }
    return tokens;
}


// Tells the engine to use the UCI interface
void UCI::uci()
{
    syncCout  << EngineTools::EngineInfo(EM_UCI) << "\n"
              << "uciok" << syncEndl;
}


// Enters the engine into debug mode
void UCI::debug(const std::vector<std::string>& args)
{
    bool invalidArgs = false;
    if (args.empty()) invalidArgs = true;
    else if (args[0] == "on") {}
    else if (args[0] == "off") {}
    else { invalidArgs = true; }
    if (invalidArgs) { sendInvalidArguments(args); }
}


// Synchronizes the engine with the GUI
void UCI::isready()
{
    /* * */
}


// Sets a parameter for an option in the engine
void UCI::setoption(const std::vector<std::string>& args)
{
    /* * */
}


// Sets up a new game in the UCI
void UCI::ucinewgame()
{
    // set up initial board
    std::shared_ptr<Position> boards(new Position());

    // In debug mode print the bitboard layout to the console
    if (Debug::getIsDebugLogEnabled())
    {
        syncCout << Debug::LogType[0]
                 << ": Printing initial board layout" << syncEndl;
        Console::printCombinedBitboards(boards);
    }
}


// Sets up the position described in the FEN string on the internal board
void UCI::position(const std::vector<std::string>& args)
{
    /* * */
}


// Performs an action based on the arguments
void UCI::go(std::vector<std::string> args)
{
    // Perform action based on the commant
    if (args.empty()) { sendInvalidArguments(args); }
    else
    {
        /*
        std::string goCmd = args[0];
        if(goCmd == "searchmoves"){ }
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "") {}
        else if(goCmd = "infinite") {}
        else { sendInvalidArguments(args); }
        */
    }
}


// Stop calculating as soon as posisble
void UCI::stop()
{
    /* * */
}


// Played an expected move
void UCI::ponderhit()
{
    /* * */
}


// Sends debug information to the CLI interface
void UCI::sendDebugInfo(const std::string& message)
{
    syncCout << "info debug " << message << syncEndl;
}


// Sends multiple debug messages to the CLI interface
void UCI::sendDebugInfo(const std::vector<std::string>& messages)
{
    for (auto& it : messages)
    { sendDebugInfo(it); }
}


// Sends invalid arguments to the command line
void UCI::sendInvalidArguments(const std::vector<std::string>& args)
{
    syncCout << "Invalid Arguments: "
             << (args.empty() ?
                     "None Provided" :
                     EngineTools::vectorToString(args)
                ) << syncEndl;
}

