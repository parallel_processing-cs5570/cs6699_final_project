/**
 * @file enginetools.cpp
 * @brief EngineTools class implementation
 * @author Paul P
 * @date 1/19/2019
 */
#include "enginetools.h"

#include <algorithm>
#include <string>
#include <vector>

#include "types.h"

// Initialize static data members of EngineTools
const std::string EngineTools::EngineName = "ISU Chess Engine";
const std::string EngineTools::AuthorList =
    "Clinton Brown, Bryant Foster, Porter Glines, Paul Grahek";


// Concatenates a list of strings, separated by a space (" ")
std::string EngineTools::vectorToString(const std::vector<std::string>& items)
{
    std::string retString = "";
    for (auto& it : items)
    {
        retString += it + " ";
    }
    return retString;
}

// Creates an all-lowercase version of the string
std::string EngineTools::stringToLower(const std::string& str)
{
    std::string retString = "";
    std::transform(str.begin(),str.end(),retString.begin(), ::tolower);
    return retString;
}


// Reurns the engine info string for UCI
std::string EngineTools::EngineInfo(EngineMode mode)
{
    // Construct the string IF in UCI mode
    std::string InfoString = "";
    if (mode == EM_UCI)
    {
        InfoString += "id name ";
        InfoString += EngineName;
        InfoString += "\n";
        InfoString += "id authors ";
        InfoString += AuthorList;
        InfoString += "\n";
    }
    else if (mode == EM_CLI)
    {
        // Nothing to do for CLI mode
    }
    return InfoString;
}

