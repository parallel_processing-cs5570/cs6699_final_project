## ========================================================================= ##
##
## Source files for ChessEngine
##
## ========================================================================= ##
cmake_minimum_required(VERSION 3.12)

set(SOURCES
  # For basic chess board
        types.cpp
        bitboard.cpp
        cli.cpp
        console.cpp
        debug.cpp
        enginetools.cpp
        main.cpp
        position.cpp
        thread.cpp
        uci.cpp
        search.cpp
        evaluation.cpp
        movegen.cpp
        move.cpp
        zhash.cpp
        transpositiontable.cpp
        window.cpp
        graphics.cpp
        button.cpp
)

add_executable(ChessEngine
  ${HEADERS}
  ${SOURCES}
  )

## ========================================================================= ##
## Load TPLs:
## ========================================================================= ##
# Include PTHREAD:
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

# Include SDL2:
find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_ttf REQUIRED)

include_directories(${SDL2_INCLUDE_DIRS}
  ${SDL2_IMAGE_INCLUDE_DIRS}
  ${SDL2_TTF_INCLUDE_DIR})

link_directories (${SDL2_LIBRARY_DIRS}
  ${SDL2_IMAGE_LIBRARY_DIRS}
  ${SDL2_TTF_LIBRARY_DIRS})


# INCLUDE(FindPkgConfig)
# pkg_check_modules(SDL2 REQUIRED sdl2)
# pkg_check_modules(SDL2_IMG REQUIRED SDL2_image)
# pkg_check_modules(SDL2_TTF REQUIRED SDL2_ttf)

#include_directories(${JUNCTION_ALL_INCLUDE_DIRS})


## ========================================================================= ##
## Link to directories
## ========================================================================= ##
target_link_libraries(ChessEngine
  INTERFACE
  cuckoo
  moodycamel
  PUBLIC
  Threads::Threads
  ${SDL2_LIBRARIES}
  ${SDL2_IMAGE_LIBRARIES}
  ${SDL2_TTF_LIBRARIES}
  )

# Link to external libraries (and SDL2) via '#include "lib_name/file"'
target_include_directories(ChessEngine
  PRIVATE
  "${CMAKE_CURRENT_SOURCE_DIR}/include"
  "${CMAKE_SOURCE_DIR}/external"
  ${SDL2_INCLUDE_DIRS}
  ${SDL2_IMAGE_INCLUDE_DIRS}
  ${SDL2_TTF_INCLUDE_DIRS}
  )


# Compile ChessEngine with the SDL2 flags
target_compile_options(ChessEngine
  PRIVATE
  "-lSDL2"
  "-lSDL2_image"
  "-lSDL2_tff"
  )

# Require C++17
target_compile_features(ChessEngine
  PUBLIC
  cxx_std_17)


## ========================================================================= ##
## Create the ChessEngine executable
## ========================================================================= ##
add_custom_command(TARGET ChessEngine PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${CMAKE_SOURCE_DIR}/external/sdlroot $<TARGET_FILE_DIR:ChessEngine>)

#add_custom_command(TARGET ChessEngine PRE_BUILD
#        COMMAND ${CMAKE_COMMAND} -E copy_directory
#        ${CMAKE_SOURCE_DIR}/bin $<TARGET_FILE_DIR:ChessEngine>)

