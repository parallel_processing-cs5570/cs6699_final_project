/**
 * @file button.cpp
 * @brief Button class implementation
 * @author Bryan T
 * @date 4/19/2019
 */
#include "button.h"

// Simple constructor
Button::Button(const double x, const double tx, const double y,
        const double ty, const double h, const double w,
        const double tw, const std::string& c, const float sc)
{
    _topLeftX = x;
    _textX = tx;
    _topLeftY = y;
    _textY = ty;
    _height = h;
    _width = w;
    _textWidth = tw;
    _content = c;
    _screenScale = sc;
}


// Returns true/false if the button was clicked
bool Button::wasClicked(const double mouseX, const double mouseY) const noexcept
{
    if(_disabled) { return false; }

    double minX = _screenScale * _topLeftX;
    double maxX = minX + (_screenScale * _width);
    double minY = _screenScale * _topLeftY;
    double maxY = minY + (_screenScale * _height);

    return (mouseX >= minX) && (mouseX <= maxX) &&
           (mouseY >= minY) && (mouseY <= maxY);
}


// Updates the clickFrameCount and clicked data as needed
void Button::updateClickData() noexcept
{
    if(_clicked && --_clickFrameCount < 0)
    {
        _clickFrameCount = 8;
        _clicked = false;
    }
}


// Returns the color of the button
SDL_Color Button::getColor() const noexcept
{
    if(_disabled)
    {
        return {178, 34, 34, 255};
    }
    else if(_clicked && _clickFrameCount > 0)
    {
        return {0, 200, 0, 255};
    }
    else
    {
        return {106, 75, 53, 255};
    }
}

