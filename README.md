# Chess Engine

Let's play chess! This project is the source code for a chess game using the min/max game theory algorithm. It seems to work, however does not use "standard" moves that may be expected.


## Purpose

The project description is as follows:

> Project 6 - analyze the Chess Code program using that uses the classic game theory min max algorithm including a class diagram. How consistent is its style? improve the class structure. Add block comments with goal statements for all classes and methods. Fix any really bad names.

Simply put, the following must be done:
+ Create Class Diagram
+ Address style: is it consistent?
+ Improve class structure
+ Generate good documentation by adding goal statements for all classes and methods
+ Address poor naming

See the acknowledgments section for details on the original source of this repository.


## Documentation

Documentation is contained internal to the project. To generate the 
documentation, simply run "doxygen doxygen.cfg".

Documentation for the project after the work was performed may be [found 
here](https://parallel_processing-cs5570.gitlab.io/cs6699_final_project/).  
Documentation for the original source code, prior to any work, is [located 
here](https://paulgrahek.gitlab.io/chessengine).


## Acknowledgments

The original repository for this project is [located here](https://gitlab.com/paulgrahek/chessengine). The work performed here is an attempt to modify the source code, excluding the external libraries, to make it "better" from a best-practices stand point.
