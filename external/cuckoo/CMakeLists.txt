## ========================================================================= ##
##
## Cuckoo library
##
## ========================================================================= ##

# Cuckoo is an interface (all headers) library target
add_library(cuckoo INTERFACE)

file(GLOB HEADERS "*.hh")
target_sources(cuckoo INTERFACE ${HEADERS})

# Include relative to the base directory
target_include_directories(cuckoo INTERFACE
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

target_compile_features(cuckoo INTERFACE cxx_std_11)

